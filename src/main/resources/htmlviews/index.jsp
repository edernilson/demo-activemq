<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Evento</title>
</head>
<body>
    
    <div id="messages"></div>


    <script>
        var messages = document.getElementById("messages");
        var eventoSource = new EventSource("/subscribe#publicaEmissor");
        eventoSource.onmessage = function(event) {
            var data = event.data;
            messages.innerHTML = data;
        }
    </script>

</body>
</html>
