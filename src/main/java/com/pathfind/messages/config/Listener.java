package com.pathfind.messages.config;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.pathfind.messages.model.JobEntidade;
import com.pathfind.messages.model.JobEntidadeRepository;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.List;

/**
 * Project: demo-activemq
 * Created by Eder Nilson on 05/05/2019.
 *
 * Dados de teste:
 * 05-05-2019 23:11;Pathfind;DIARIO
 * 05-05-2019 23:11;ViaSolut;MENSAL
 *
 */
@Component
public class Listener {

    private JobEntidadeRepository repo;

    public Listener(JobEntidadeRepository repo) {
        this.repo = repo;
    }

    @JmsListener(destination = "retroalimentacao.topic")
    public void receiveMessage(final Message jsonMessage) throws JMSException {
        String[] data = null;
        if (jsonMessage instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) jsonMessage;
            data = textMessage.getText().split(";");

            //Remove cliente
            repo.deleteAllByCliente(data[1]);

            //Tem periodicidade
            if (data.length > 2){
                JobEntidade jobEntidade = new JobEntidade(data[1], data[2]);
                repo.save(jobEntidade);
            }
            List<JobEntidade> lista = repo.findAll();
            System.out.println("-------------------------------------------------");
            for (JobEntidade entidade: lista) {
                System.out.println(String.format("Cliente: %s - periodicidade: %s", entidade.getCliente(), entidade.getPeriodicidade()));
            }
        }
    }
    
}
