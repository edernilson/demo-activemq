package com.pathfind.messages.config;

import lombok.extern.java.Log;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.pathfind.messages.model.JobEntidade;
import com.pathfind.messages.model.JobEntidadeRepository;

import java.util.stream.Stream;

import static java.lang.System.out;

/**
 * Project: demo-activemq
 * Created by Eder Nilson on 05/05/2019.
 */
@Log
@Component
@Transactional(readOnly = true)
public class Jobs {

    private JobEntidadeRepository repo;

    public Jobs(JobEntidadeRepository repo) {
        this.repo = repo;
    }

    @Scheduled(cron = "${cron.diario}")
    public void diario() {

        //log.info("Diario");

        try (Stream<JobEntidade> jobs = repo.findAllByPeriodicidade("DIARIO")) {
            jobs.forEach(job -> {
                out.printf("Executando job para o cliente: %s%n", job.getCliente().toUpperCase());
            });
        } catch (Exception e) {
            log.info(String.format("Exception ocorrido: %s", e.getMessage()));
            throw new RuntimeException("Exception ocorrido no Job Diario", e);
        }
    }

    @Scheduled(cron = "${cron.mensal}")
    public void mensal() {
        //log.info("Mensal");
    }

    @Scheduled(cron = "${cron.trimestral}")
    public void trimetral() {
        //log.info("Trimestral");
    }

}
