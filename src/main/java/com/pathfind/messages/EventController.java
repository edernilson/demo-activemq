package com.pathfind.messages;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Controller
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/")
public class EventController {

	private Set<SseEmitter> sseEmitters = new HashSet<SseEmitter>();
	private int messageCount = 0;

	@RequestMapping("/")
    public String index() {
    	return "index";
    }
	
	@RequestMapping("/teste")
	public void teste() throws IOException {
		String msg = "Teste mensagem";

		final SseEmitter sseEmitter = new SseEmitter();

        sseEmitter.onCompletion(() -> {
            synchronized (this.sseEmitters) {
                this.sseEmitters.remove(sseEmitter);
            }
        });

        sseEmitter.onTimeout(()-> {
            sseEmitter.complete();
        });		
        
        sseEmitter.send(msg);                        
        sseEmitter.complete();
		
	}
	
	@RequestMapping("/home")
    public ModelAndView home() {
    	ModelAndView andView = new ModelAndView();
    	andView.setViewName("index");
		return andView;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
	@GetMapping("/subscribe")
	public SseEmitter publicaEmissor() {
		
		final SseEmitter sseEmitter = new SseEmitter();

        sseEmitter.onCompletion(() -> {
            synchronized (this.sseEmitters) {
                this.sseEmitters.remove(sseEmitter);
            }
        });

        sseEmitter.onTimeout(()-> {
            sseEmitter.complete();
        });
		
        this.sseEmitters.add(sseEmitter);		
		
		return sseEmitter;
	}
	
    @JmsListener(destination = "teste.fila")
    public void receiveAndSendMessage(final Message jsonMessage) {
    	System.out.println("Mensagem: "+ jsonMessage);
    	if (jsonMessage instanceof TextMessage) {
    		
            sseEmitters.forEach(emitter -> {
                if (null != emitter)
                    try {
                        emitter.send(((TextMessage) jsonMessage).getText());                        
                        //Thread.sleep(1000);
                        //emitter.complete();
                        
                    } catch (IOException | JMSException e) {
                        e.printStackTrace();
                    }
            });    		
    	}
    }
    
}
