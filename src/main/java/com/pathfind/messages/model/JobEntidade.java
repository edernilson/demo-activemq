package com.pathfind.messages.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Project: demo-activemq
 * Created by Eder Nilson on 05/05/2019.
 */
@EqualsAndHashCode(of = "id")
@Data
@Entity
public class JobEntidade {

    @Id
    @GeneratedValue()
    private Long id;
    private String cliente;
    private String periodicidade;

    public JobEntidade() {
    }

    public JobEntidade(String cliente, String periodicidade) {
        this.cliente = cliente;
        this.periodicidade = periodicidade;
    }
}
