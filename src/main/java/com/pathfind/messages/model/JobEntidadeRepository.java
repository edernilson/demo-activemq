package com.pathfind.messages.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Stream;

/**
 * Project: demo-activemq
 * Created by Eder Nilson on 05/05/2019.
 */
@Transactional
@Repository
public interface JobEntidadeRepository extends JpaRepository<JobEntidade, Long> {
    Stream<JobEntidade> findAllByPeriodicidade(String periodicidade);

    void deleteAllByCliente(String cliente);
}
