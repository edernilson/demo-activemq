FROM tomcat

#ADICIONADO LIBS
#ADD lib/ /usr/local/tomcat/lib
 
#ADICIONANDO USU�RIOS
#ADD tomcat-users.xml /usr/local/tomcat/conf/

#ADICIONANDO SERVER
#ADD server.xml /usr/local/tomcat/conf/

#ADICIONANDO CONFIGURACOES DE MEMORIA
#ADD setenv.sh /usr/local/tomcat/bin/

#ADICIONANDO .WAR
#ADD pathfind_teste.war /usr/local/tomcat/webapps/
#ADD pathfind_atlantica_pi_tes.war /usr/local/tomcat/webapps/
#ADD pathfind_mvp.war /usr/local/tomcat/webapps/
ADD ptf-activemq-sse-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/	

CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]

